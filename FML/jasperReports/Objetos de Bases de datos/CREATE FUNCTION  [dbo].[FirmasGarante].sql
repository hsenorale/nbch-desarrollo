-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create FUNCTION  [dbo].[FirmasGarante]
( -- Parametros de entrada
    @NROSOLICITUD NUMERIC(15,0)
)
RETURNS VARCHAR(800)
AS
BEGIN
	 DECLARE @cuerpo VARCHAR (800)
	 SET @cuerpo =''
	 DECLARE @CodClien NUMERIC(15,0)
	 DECLARE c_cursor01 CURSOR LOCAL FOR
	 -- consulta sql
	SELECT GP.IDPERSONA FROM
	 (
	 SELECT rd.IDPERSONA
	 ,'TIPO'= CASE  WHEN RD.IDPERSONA = SL22.C5187  THEN 'DEUDOR' ELSE 'GARANTE' END
				FROM   GR_RELACIONGTIACREDSOLIC RG
				INNER JOIN  GR_GARANTIAS GA ON GA.NROGARANTIA = RG.NROGARANTIA
				INNER JOIN CL_PERSONASFISICAS CL ON CL.IDPERSONA =GA.IDPERSONATITULAR
				INNER JOIN CL_RELPERDOC RD ON RD.IDPERSONA =GA.IDPERSONATITULAR  
				LEFT JOIN SL_SOLICITUDCREDITO SL22 ON SL22.C5000 =RG.NROSOLICITUD AND SL22.C5187 =RD.IDPERSONA
			   WHERE  RG.NROSOLICITUD = @NROSOLICITUD
				AND    RD.ESTADO = 'A'
			   AND    RD.PRINCIPAL = 'S'
			   AND    GA.SUBCLASGTIAS IN (2, 3, 4, 5, 9, 10, 11, 12)
			   AND    RG.TZ_LOCK = 0
			   AND    GA.TZ_LOCK = 0
			   AND    CL.TZ_LOCK = 0
			   AND    RD.TZ_LOCK = 0
	 )GP		 
	WHERE GP.TIPO ='GARANTE'
	GROUP BY GP.IDPERSONA,GP.TIPO
	  OPEN c_cursor01
      --WHILE (0 = 0) 
      --BEGIN 
        FETCH NEXT FROM c_cursor01 INTO @CodClien        
        SET @cuerpo = (SELECT  @cuerpo + dbo.FirmasClienteSolidario(@CodClien, 2))  + CHAR(10)
      --  IF (@@FETCH_STATUS = -1) 
        --  BREAK
      --END
      CLOSE c_cursor01
 
  RETURN @cuerpo
END
GO
SELECT ( dbo.FirmasClienteSolidario(6747416, 2))
SELECT [dbo].[FirmasGarante](52141159551)
SELECT [dbo].[FirmasGarante](51141156645)


--- comprobacion
	SELECT GP.IDPERSONA FROM
	 (
	 SELECT rd.IDPERSONA
	 ,'TIPO'= CASE  WHEN RD.IDPERSONA = SL22.C5187  THEN 'DEUDOR' ELSE 'GARANTE' END
				FROM   GR_RELACIONGTIACREDSOLIC RG
				INNER JOIN  GR_GARANTIAS GA ON GA.NROGARANTIA = RG.NROGARANTIA
				INNER JOIN CL_PERSONASFISICAS CL ON CL.IDPERSONA =GA.IDPERSONATITULAR
				INNER JOIN CL_RELPERDOC RD ON RD.IDPERSONA =GA.IDPERSONATITULAR  
				LEFT JOIN SL_SOLICITUDCREDITO SL22 ON SL22.C5000 =RG.NROSOLICITUD AND SL22.C5187 =RD.IDPERSONA
			   WHERE  RG.NROSOLICITUD = 52141159551
				AND    RD.ESTADO = 'A'
			   AND    RD.PRINCIPAL = 'S'
			   AND    GA.SUBCLASGTIAS IN (2, 3, 4, 5, 9, 10, 11, 12)
			   AND    RG.TZ_LOCK = 0
			   AND    GA.TZ_LOCK = 0
			   AND    CL.TZ_LOCK = 0
			   AND    RD.TZ_LOCK = 0
	 )GP		 
	WHERE GP.TIPO ='GARANTE'
	GROUP BY GP.IDPERSONA,GP.TIPO