USE [Topaz_Desa]
GO
/****** Object:  UserDefinedFunction [dbo].[FirmasClienteSolidario]    Script Date: 20/02/2015 05:34:55 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[FirmasClienteSolidario]
(
   @CodClien NUMERIC(15,0),
   @FlgInfor NUMERIC(1,0)
)
RETURNS VARCHAR(800)
AS
BEGIN
  DECLARE @wauxgrupsol VARCHAR (400)
  DECLARE @wnombrepers VARCHAR (50)
  DECLARE @wdescestciv VARCHAR (30)
  DECLARE @wdescprofes VARCHAR (60)
  DECLARE @wdescdirecc VARCHAR (80)
  DECLARE @wnumdocumen VARCHAR (20)
  
  SET @wauxgrupsol = ' '
  IF @FlgInfor = 0
    BEGIN
      DECLARE c_cursor01 CURSOR LOCAL FOR 
        SELECT (CL.C1000) AS Nombre,
               (SELECT OP.DESCRIPCION FROM OPCIONES OP WHERE OP.NUMERODECAMPO = 1409 AND OP.OPCIONINTERNA = PF.C1409 AND OP.IDIOMA = 'E') AS EstCivil,
               (SELECT PR.C1222 FROM TC_PROFESIONES PR WHERE PR.C1221 = PF.C1499 AND PR.TZ_LOCK = 0) AS Profesion,
               (SELECT (D3.C1216 + ', ' + DI.REFERENCIA) FROM TC_DIRECCIONES DI, TC_DIR_DIM3 D3 
                WHERE  DI.ID = CL.C0902 AND DI.FORMATO = 'C' AND DI.TIPODIRECCION = 'L' AND D3.C1212 = DI.DIM1 AND D3.C1214 = DI.DIM2
                AND    D3.C1215 = DI.DIM3 AND DI.TZ_LOCK = 0 AND D3.TZ_LOCK = 0) AS Direccion,
               (SELECT RD.NRODOCUMENTO FROM CL_RELPERDOC RD WHERE  RD.IDPERSONA = PF.IDPERSONA AND RD.PRINCIPAL = 'S' AND RD.ESTADO = 'A' AND RD.TZ_LOCK = 0) AS NroDocumento
        FROM   CL_CLIENTES CL, CL_CLIENTPERSONA CP, CL_PERSONASFISICAS PF
        WHERE  CL.C0902 = @CodClien
        AND    CP.C1430 = CL.C0902
        AND    PF.IDPERSONA = CP.IDPERSONA
        AND    PF.TZ_LOCK = 0
        AND    CP.TZ_LOCK = 0
        AND    CL.TZ_LOCK = 0
      OPEN c_cursor01
      WHILE (0 = 0) 
      BEGIN 
        FETCH NEXT FROM c_cursor01 INTO @wnombrepers, @wdescestciv, @wdescprofes, @wdescdirecc, @wnumdocumen
        IF (@@FETCH_STATUS = -1) 
          BREAK
        
        SET @wauxgrupsol = @wauxgrupsol + '------------------------------' + char(10)
        SET @wauxgrupsol = @wauxgrupsol + '(f) Por EL DEUDOR:'+ char(10)
        SET @wauxgrupsol = @wauxgrupsol + @wnombrepers + char(10)
       /* Autor: Alvaro Dax
       *  Fecha modificación: 21/02/2015
       *  se comentario este segmento de la firma por que la version de los ultimos Contratos no requieren 
       *  esta información
       * */
        --SET @wauxgrupsol = @wauxgrupsol + 'Mayor de edad, de profesión u oficio: ' + @wdescprofes + char(10)
        --SET @wauxgrupsol = @wauxgrupsol + 'Domicilio: ' + @wdescdirecc + char(10)
        --SET @wauxgrupsol = @wauxgrupsol + 'Cédula de identidad: ' + @wnumdocumen        
        
      END
      CLOSE c_cursor01
    END
  IF @FlgInfor = 1
    BEGIN
      DECLARE c_cursor02 CURSOR LOCAL FOR 
        SELECT (SELECT CL.C1000 
                FROM   CL_CLIENTES CL 
                WHERE  CL.C0902 = GA.IDPERSONATITULAR 
                AND    CL.TZ_LOCK = 0) AS Nombre,
               (SELECT OP.DESCRIPCION 
                FROM   CL_PERSONASFISICAS PF, OPCIONES OP 
                WHERE  PF.IDPERSONA = GA.IDPERSONATITULAR 
                AND    OP.NUMERODECAMPO = 1409
                AND    OP.IDIOMA = 'E'
                AND    OP.OPCIONINTERNA= PF.C1409
                AND    PF.TZ_LOCK = 0) AS EstCivil,
               (SELECT PR.C1222 
                FROM   CL_PERSONASFISICAS PF, TC_PROFESIONES PR 
                WHERE  PF.IDPERSONA = GA.IDPERSONATITULAR 
                AND    PF.C1499 = PR.C1221
                AND    PF.TZ_LOCK = 0
                AND    PR.TZ_LOCK = 0) AS Profesion,
               (SELECT (D3.C1216 + ', ' + DI.REFERENCIA) 
                FROM   TC_DIRECCIONES DI, TC_DIR_DIM3 D3 
                WHERE  DI.ID = GA.IDPERSONATITULAR
                AND    DI.FORMATO = 'C' 
                AND    DI.TIPODIRECCION = 'L' 
                AND    D3.C1212 = DI.DIM1 
                AND    D3.C1214 = DI.DIM2
                AND    D3.C1215 = DI.DIM3 
                AND    DI.TZ_LOCK = 0 
                AND    D3.TZ_LOCK = 0) AS Direccion,
               (SELECT RD.NRODOCUMENTO
                FROM   CL_RELPERDOC RD
                WHERE  RD.IDPERSONA = GA.IDPERSONATITULAR
                AND    RD.PRINCIPAL = 'S' 
                AND    RD.ESTADO = 'A' 
                AND    RD.TZ_LOCK = 0) AS NroDocumento
        FROM   GR_RELACIONGTIACREDSOLIC RG, GR_GARANTIAS GA
        WHERE  RG.NROSOLICITUD = @CodClien
        AND    RG.NROGARANTIA = GA.NROGARANTIA
        AND    GA.SUBCLASGTIAS = 8
        AND    RG.TZ_LOCK = 0
        AND    GA.TZ_LOCK = 0
      OPEN c_cursor02
      WHILE (0 = 0) 
      BEGIN 
        FETCH NEXT FROM c_cursor02 INTO @wnombrepers, @wdescestciv, @wdescprofes, @wdescdirecc, @wnumdocumen
        IF (@@FETCH_STATUS = -1) 
          BREAK
        
        
        SET @wauxgrupsol = @wauxgrupsol + '------------------------------' + char(10)
        SET @wauxgrupsol = @wauxgrupsol + '(f) Por EL FIADOR:'+ char(10)
        SET @wauxgrupsol = @wauxgrupsol + @wnombrepers + char(10)
       /* Autor: Alvaro Dax
       *  Fecha modificación: 21/02/2015
       *  se comentario este segmento de la firma por que la version de los ultimos Contratos no requieren 
       *  esta información
       * */
       -- SET @wauxgrupsol = @wauxgrupsol + '------------------------------' + char(10)
       -- SET @wauxgrupsol = @wauxgrupsol + '(f) Por EL FIADOR: '+@wnombrepers + char(10)
       -- SET @wauxgrupsol = @wauxgrupsol + 'Mayor de edad, de profesión u oficio: ' + @wdescprofes + char(10)
       -- SET @wauxgrupsol = @wauxgrupsol + 'Domicilio: ' + @wdescdirecc + char(10)
       -- SET @wauxgrupsol = @wauxgrupsol + 'Cédula de identidad: ' + @wnumdocumen       
       
      END
      CLOSE c_cursor02
    END
     -- Parte agregada por Alvaro Dax Diaz Amaya
     -- Fecha de adicion:20/02/2015
     IF @FlgInfor = 2
    BEGIN
      DECLARE c_cursor01 CURSOR LOCAL FOR 
        SELECT rtrim(LTRIM((pf.C1404 +' '+ pf.C1405 +' '+ pf.C1406 )))  AS Nombre,
               (SELECT OP.DESCRIPCION FROM OPCIONES OP WHERE OP.NUMERODECAMPO = 1409 AND OP.OPCIONINTERNA = PF.C1409 AND OP.IDIOMA = 'E') AS EstCivil,
               (SELECT PR.C1222 FROM TC_PROFESIONES PR WHERE PR.C1221 = PF.C1499 AND PR.TZ_LOCK = 0) AS Profesion,
               (SELECT RD.NRODOCUMENTO FROM CL_RELPERDOC RD WHERE  RD.IDPERSONA = PF.IDPERSONA AND RD.PRINCIPAL = 'S' AND RD.ESTADO = 'A' AND RD.TZ_LOCK = 0) AS NroDocumento
        FROM   CL_PERSONASFISICAS PF
        WHERE  pf.IDPERSONA = @CodClien
        AND    PF.TZ_LOCK = 0      
      OPEN c_cursor01
      WHILE (0 = 0) 
      BEGIN 
        FETCH NEXT FROM c_cursor01 INTO @wnombrepers, @wdescestciv, @wdescprofes,  @wnumdocumen
        IF (@@FETCH_STATUS = -1) 
          BREAK
        
        SET @wauxgrupsol = @wauxgrupsol + '------------------------------' + char(10)
        SET @wauxgrupsol = @wauxgrupsol + '(f) Por EL GARANTE PRENDARIO:'+ char(10)
        SET @wauxgrupsol = @wauxgrupsol + @wnombrepers + char(10)
       -- SET @wauxgrupsol = @wauxgrupsol + 'Mayor de edad, de profesión u oficio: ' + @wdescprofes + char(10)
       -- SET @wauxgrupsol = @wauxgrupsol + 'Domicilio: ' + @wdescdirecc + char(10)
       -- SET @wauxgrupsol = @wauxgrupsol + 'Cédula de identidad: ' + @wnumdocumen
              
      END
      CLOSE c_cursor01
    END 
    
  
  RETURN @wauxgrupsol
END