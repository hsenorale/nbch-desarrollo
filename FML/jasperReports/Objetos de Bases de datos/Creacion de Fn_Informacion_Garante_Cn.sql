
create FUNCTION Fn_Informacion_Garante_Cn
	/*
	* Autor: Alvaro Dax Diaz Amaya
	* Descripcion: Esta funcion es contiene la información  de los Garantes Prendarios,
	* dicha información es utilizado en los contratos Privados que se generar en archivos Jasper
	* En los archivos fisicos Q9950 - Q9956
	* Fecha de creacion: 20/02/2015 
	* */
	(
    -- declaracion de parametro
    @NROSOLICITUD NUMERIC(15,0)
	)
RETURNS @RESULTADO TABLE (
	NumeroGarantes INT,
	InformacionGarante NVARCHAR(800),
	DEUDOR_SN CHAR(2)
	)
BEGIN
		-- DECLARACIONES DE VARIABLES INTERNA
		DECLARE @TABLA_RESULTADO TABLE (id_pk NUMERIC IDENTITY,IDPERSONA NUMERIC(15,0), TIPO NVARCHAR(50))  
		
		INSERT INTO @TABLA_RESULTADO
		 SELECT GP.IDPERSONA,GP.TIPO FROM
 (
 SELECT rd.IDPERSONA
 ,'TIPO'= CASE  WHEN RD.IDPERSONA = SL22.C5187  THEN 'DEUDOR' ELSE 'GARANTE' END
            FROM   GR_RELACIONGTIACREDSOLIC RG
            INNER JOIN  GR_GARANTIAS GA ON GA.NROGARANTIA = RG.NROGARANTIA
            INNER JOIN CL_PERSONASFISICAS CL ON CL.IDPERSONA =GA.IDPERSONATITULAR
            INNER JOIN CL_RELPERDOC RD ON RD.IDPERSONA =GA.IDPERSONATITULAR  
            LEFT JOIN SL_SOLICITUDCREDITO SL22 ON SL22.C5000 =RG.NROSOLICITUD AND SL22.C5187 =RD.IDPERSONA
           WHERE  RG.NROSOLICITUD = @NROSOLICITUD
            AND    RD.ESTADO = 'A'
           AND    RD.PRINCIPAL = 'S'
           AND    GA.SUBCLASGTIAS IN (2, 3, 4, 5, 9, 10, 11, 12)
           AND    RG.TZ_LOCK = 0
           AND    GA.TZ_LOCK = 0
           AND    CL.TZ_LOCK = 0
           AND    RD.TZ_LOCK = 0
 )GP		 
  GROUP BY GP.IDPERSONA,GP.TIPO

	-- INDICADOR DE LAS SIGUIENTES SITUACIONES
	/*
	*  DEUDOR Y GARANTE
	*  UNICAMENTE GARANTE
	* */
	DECLARE @DEUDOR_SN CHAR(2)
		     IF( (SELECT COUNT( tr.TIPO) FROM @TABLA_RESULTADO tr WHERE tr.TIPO ='DEUDOR') =1	)
		     BEGIN
		     	SET @DEUDOR_SN ='SI'
		     END
		     ELSE
		     BEGIN
		     	SET @DEUDOR_SN='NO'		
		     END
	DECLARE @NumeroGarantes AS INT
		SET @NumeroGarantes =(SELECT COUNT( DISTINCT tr.IDPERSONA) FROM @TABLA_RESULTADO tr WHERE tr.TIPO ='GARANTE')			
		 
		    	
	-- declaraciones para realizar la iteracion entre los Garantes
	DECLARE @i AS INT
			,@rows AS INT
			,@Cuerpo NVARCHAR(800) 
			
	-- inicializacion de las variables para la iteracion		
	SET @i =1
	SET @rows = (SELECT MAX(tr.id_pk) FROM 	@TABLA_RESULTADO tr) 
	SET @Cuerpo =''
	WHILE (@rows >=@i)
	BEGIN
		-- variables internas dentro del ciclo while
		
			declare	 @IDPERSONA NUMERIC(15,0)			
			   
		SET @Cuerpo =' y '
			-- Realizando la consulta iterativa
			SELECT @IDPERSONA=	tr.IDPERSONA
			FROM
				@TABLA_RESULTADO tr
			WHERE tr.id_pk =@i AND TR.TIPO ='GARANTE'
			
		SET @Cuerpo = @Cuerpo + (SELECT dbo.InformacionClienteFianza(@IDPERSONA, 0))  
		SET @Cuerpo =@Cuerpo + ' (a quien en lo sucesivo se denominará como EL GARANTE PRENDARIO ) ' 
		-- se incrementa el contador
		SET @i = @i +1
	END
		-- Final
		IF ((SELECT COUNT(*) FROM  @TABLA_RESULTADO)>0) 
		BEGIN
					
					
					INSERT INTO @RESULTADO												
					SELECT @NumeroGarantes, @Cuerpo, @DEUDOR_SN  
		END
		ELSE
			BEGIN
				   INSERT INTO @RESULTADO
					SELECT 0,'',''
			END

		return	
END
GO

SELECT * FROM DBO.Fn_Informacion_Garante_Cn(51141156645)
SELECT * FROM DBO.Fn_Informacion_Garante_Cn(52141163554)
SELECT * FROM DBO.Fn_Informacion_Garante_Cn(52141159551)

SELECT (dbo.FirmasClienteSolidario(51110625, 0))
  SELECT (dbo.FirmasClienteSolidario(100041, 2))
  
		 SELECT GP.IDPERSONA,GP.TIPO FROM
 (
 SELECT rd.IDPERSONA
 ,'TIPO'= CASE  WHEN RD.IDPERSONA = SL22.C5187  THEN 'DEUDOR' ELSE 'GARANTE' END
            FROM   GR_RELACIONGTIACREDSOLIC RG
            INNER JOIN  GR_GARANTIAS GA ON GA.NROGARANTIA = RG.NROGARANTIA
            INNER JOIN CL_PERSONASFISICAS CL ON CL.IDPERSONA =GA.IDPERSONATITULAR
            INNER JOIN CL_RELPERDOC RD ON RD.IDPERSONA =GA.IDPERSONATITULAR  
            LEFT JOIN SL_SOLICITUDCREDITO SL22 ON SL22.C5000 =RG.NROSOLICITUD AND SL22.C5187 =RD.IDPERSONA
           WHERE  RG.NROSOLICITUD = 51141156645
            AND    RD.ESTADO = 'A'
           AND    RD.PRINCIPAL = 'S'
           AND    GA.SUBCLASGTIAS IN (2, 3, 4, 5, 9, 10, 11, 12)
           AND    RG.TZ_LOCK = 0
           AND    GA.TZ_LOCK = 0
           AND    CL.TZ_LOCK = 0
           AND    RD.TZ_LOCK = 0
 )GP		 
  GROUP BY GP.IDPERSONA,GP.TIPO